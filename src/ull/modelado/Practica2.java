package ull.modelado;

import java.util.Scanner;

import ull.modelado.fraccion.Fraction;
/**
 * @author Orlandy Ariel S�nchez A.
 * Clase para probar el funcionamiento de la pr�ctica
 */
public class Practica2
{
    public static void main(String[] args) throws CloneNotSupportedException
    {
    	//Practica2 t_p = new Practica2();
    	
    	//t_p.implMax();
        //System.err.println(Fraction.gcd(4, 0));
        //Fraction t_f = new Fraction("   4     ");
        //t_f.setM_denominador(-4);
        //System.out.println(t_f);
        Fraction aa = new Fraction(4, 5);
        Fraction a = new Fraction(7,10);
        a = aa.minus(a);
        System.out.println(a);
        
        a.setM_numerador(2);
        System.out.println(a);
    }
    public void implMax()
    {
        Fraction t_f1 = new Fraction(3, 5);
        Fraction t_f2 = new Fraction(4, 3);
        
        Fraction t_aux =t_f1.add(t_f2).power(2);
        t_aux.changeSign();
        System.out.println(t_aux);
        System.out.println("El maximo de entre "+t_f1+" y "+t_f2+", es:" + Fraction.maximoFracciones(t_f1, t_f2));
    }
    public void fraccion() throws CloneNotSupportedException
    {
        Fraction t_frac1;
        Fraction t_frac2;
        long t_numerador = 0;
        long t_denominaaor = 0;
        Scanner t_sc = new Scanner(System.in);
        System.out.print("Introduce el numerador de la primera fracci�n:");
        t_numerador = t_sc.nextLong();
        System.out.print("Introduce el denominador de la primera fracci�n:");
        t_denominaaor = t_sc.nextLong();
        t_frac1 = new Fraction(t_numerador, t_denominaaor);
        System.out.print("Introduce el numerador de la segunda fracci�n:");
        t_numerador =0;
        t_numerador = t_sc.nextLong();
        
        System.out.print("Introduce el denominador de la segunda fracci�n:");
        t_denominaaor = 0;
        t_denominaaor = t_sc.nextLong();
        t_sc.close();
        t_frac2 = new Fraction(t_numerador, t_denominaaor);
        System.out.println("Suma:\n"+ t_frac1.add(t_frac2));
        System.out.println("Resta:\n"+ t_frac1.minus(t_frac2));
        System.out.println("Producto:\n"+ t_frac1.multiply(t_frac2));
        System.out.println("Cociente:\n"+ t_frac1.divide(t_frac2));
        
    }
}
