package ull.modelado.fraccion;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Orlandy Ariel S�nchez A.
 * Clase que representa una fracci�n
 */
public class Fraction
{   // Atributos
    private Long m_numerador, m_denominador;
    // Constructor/es y m�todos
    /**
     * Constructor por defecto, se crear� con la fracci�n 0/1
     */
    public Fraction()
    {
        this(0L,1L);
    }
    /**
     * Constructor que recibe como argumento el n�merador, por defecto 
     * su denominador es 1
     * @param a_numerador
     */
    public Fraction(long a_numerador)
    {
    	this(a_numerador,1L);
    }
    /**
     * Constructor que recibe el numerador y el denominador 
     * para construir una fracci�n
     * @param a_numerador
     * @param a_denominador
     */
    public Fraction(long a_numerador, long a_denominador)
    {
        m_numerador = a_numerador;
        //setM_denominador(a_denominador);
        m_denominador = a_denominador;
        //normaliza().simplificar();
        simplificar();
        normaliza();
    }
    /**
     * Constructor que crea una fracci�n a partir de un n�mero decimal 
     * que recibe como argumento
     * @param a_fraccion
     */
    public Fraction(double a_fraccion)
    {
        this(parseDoubleString(a_fraccion));
    }
    /**
     * Constructor que recibe una cadena de texto con el siguiente formato: num/den
     * para representar una fracci�n
     * @param a_fraccion
     */
    public Fraction(String a_fraccion)
    {
        set_fraction(parseFraction(a_fraccion));
    }
    /**
     * Funci�n que transforma una cadena en una fracci�n
     * @param a_fraccion
     * @return
     */
    public static Fraction parseFraction(String a_fraction)
    {
        Fraction t_auxFr = new Fraction();
        Matcher matcher ;
        Pattern patron;
        
        if (a_fraction.matches("(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*\\/(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*"))
        {
            patron = Pattern.compile("(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*\\/(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*");
            matcher = patron.matcher(a_fraction);
            matcher.find();
            String t_group1 = matcher.group(2);
            String t_group3 = matcher.group(8);
            long t_auxNum = 0;
            if (t_group1 != null)
                t_auxNum = Long.parseLong(matcher.group(4)) * (-1);
            else if (t_group3 != null)
                t_auxNum = Long.parseLong(matcher.group(4)) * (-1);
            else
                t_auxNum = Long.parseLong(matcher.group(4));
            t_auxFr = new Fraction(t_auxNum,Long.parseLong(matcher.group(9)));
        }
        else if (a_fraction.matches("(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*"))
        {
            patron = Pattern.compile("(\\s)*(-|\\+)?(\\s)*(\\d+)(\\s)*");
            matcher = patron.matcher(a_fraction);
            matcher.find();
            
            t_auxFr = new Fraction(Long.parseLong(matcher.group(4)));
        }
        else
            throw new NumberFormatException("\nEl String no tiene el formato de una fracci�n #/#");
        return t_auxFr;
    }
    /**
     * Funci�n que transforma un n�mero decimal en fracci�n
     * @param a_num
     * @return
     */
    private static String parseDoubleString(Double a_num)
    {
        //expresi�n regular que separa el n�mero decimal en 2 partes
        Pattern patron = Pattern.compile("(-)?(\\d+)\\.(\\d+)");
        Matcher matcher = patron.matcher(a_num.toString());
        String t_arreglo;
        matcher.find();
        int t_aux = 1;
        for (int i = 0; i < matcher.group(3).length(); i++)
            t_aux *=10;
        if(a_num < 0)
            t_arreglo = ((int)((a_num*t_aux)) + "/" + t_aux);
        else
            t_arreglo = ((int)(a_num*t_aux)) + "/" + t_aux;
        
        return  t_arreglo;
    }
    /**
     * M�todo para obtener el signo de la fracci�n
     * @return -1 si es negativo, 1 si es positivo o 0 en cualquier otro caso
     */
    public int sign()
    {
    	if(getM_numerador() < 0)
    		return -1;
    	else if(getM_numerador() >= 0)
    		return 1;
    	else
    		return 0;
    }
    /**
     * M�todo para cambiar de signo la fracci�n
     */
    public Fraction changeSign()
    {
    	this.m_numerador *= -1;
    	return this;
    }
    /**
     * M�todo que calcula el equivalente a decimal de la fracci�n
     * @return el n�mero en decimal de la fracci�n
     */
    public double ratio()
    {
    	return (double) m_numerador / (double) m_denominador;
    }
    /**
     * M�todo M�ximo Com�n Divisor que calcula el mayor de todos los n�meros que dividen a 2 n�meros en este caso
     * @param a_numerador
     * @param a_den
     * @return
     */
    public static long gcd(long a_numeroA, long a_numeroB)
	{
        long aux;
        if(a_numeroA != 0 && a_numeroB != 0)
        {
    		while((aux = a_numeroA % a_numeroB) != 0)
    		{
    			a_numeroA = a_numeroB;
    			a_numeroB = aux;
    		}
    	}else
    	{
    	    return a_numeroA == 0 ? a_numeroB : a_numeroA;
    	}
		return a_numeroB;
		/*
		 long aux;
	        while(a_numeroB != 0)
	        {
	            aux = a_numeroA % a_numeroB;
	            a_numeroA = a_numeroB;
	            a_numeroB = aux;
	        }
		return a_numeroA;*/
	}
    /**
     * M�todo del M�nimo Com�n M�ltiplo, calcula el m�s peque�o de los n�meros que son m�ltiplos 
     * de 2, en este caso, n�meros
     * @param a_numerador
     * @param a_denominador
     * @return
     */
    public static long lcm(long a_numeroA, long a_numeroB)
    {
    	//return a_numeroA / gcd(a_numeroA,a_numeroB) * a_numeroB;
        return (a_numeroA * a_numeroB / gcd(a_numeroA, a_numeroB));
    }
    /**
     * M�todo para sumar 2 fracciones
     * @param a_fraccion recibe una fracci�n 
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction add(Fraction a_fraccion)
    {
    	long t_den = lcm(getM_denominador(), a_fraccion.getM_denominador());
    	long t_num1 = m_numerador * t_den / m_denominador;
    	long t_num2 = a_fraccion.getM_numerador() * t_den / a_fraccion.getM_denominador() ;
    	
    	return new Fraction( t_num1 + t_num2, t_den);
    }
    /**
     * M�todo para restar 2 fracciones
     * @param a_fraccion recibe una fracci�n 
     * @return una Fracci�n como resultado de la operaci�n realizada
     * @throws CloneNotSupportedException 
     */
    public Fraction minus(Fraction a_fraccion) throws CloneNotSupportedException
    {
        //long t_den = lcm(getM_denominador(), a_fraccion.getM_denominador());
        //long t_num1 = m_numerador * t_den / m_denominador;
        //long t_num2 = a_fraccion.getM_numerador() * t_den / a_fraccion.getM_denominador() ;
        
        //return new Fraction( t_num1 - t_num2, t_den);
        return add(a_fraccion.clone().changeSign());
    }
    /**
     * M�todo para multiplicar 2 fracciones
     * @param a_fraccion recibe una fracci�n 
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction multiply(Fraction a_fraccion)
    {
    	return new Fraction(m_numerador * a_fraccion.getM_numerador(), m_denominador * a_fraccion.getM_denominador());
    }
    /**
     * M�todo para dividir 2 fracciones
     * @param a_fraccion recibe una fracci�n 
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction divide(Fraction a_fraccion)
    {
    	return new Fraction(getM_numerador() * a_fraccion.getM_denominador(), getM_denominador() * a_fraccion.getM_numerador());
    }
    /**
     * M�todo para sumar una fracci�n por un n�mero
     * @param a_num recibe un n�mero con el que se sumar�
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction add(long a_num)
    {
    	return new Fraction((m_denominador*a_num)+ m_numerador,m_denominador);
    }
    /**
     * M�todo para restar una fracci�n por un n�mero
     * @param a_num recibe un n�mero con el que se restar�
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction minus(long a_num)
    {
    	return new Fraction((m_denominador*a_num)- m_numerador,m_denominador);
    }
    /**
     * M�todo para multiplicar una fracci�n por un n�mero
     * @param a_num recibe un n�mero con el que se multiplicar�
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction multiply(long a_num)
    {
    	return new Fraction(m_numerador * a_num, m_denominador);
    }
    /**
     * M�todo para dividir una fracci�n por un n�mero
     * @param a_num recibe un n�mero con el que se dividir�
     * @return una Fracci�n como resultado de la operaci�n realizada
     */
    public Fraction divide(long a_num)
    {
    	return new Fraction(m_numerador, m_denominador * a_num);
    }
    /**
     * M�todo que invierte la fracci�n
     * @TODO Preguntar si tiene que ser sobre la que se llama o 
     * si puedo usarla estatica para cualquier otra fracci�n
     */
    public Fraction reciprocal()
    {
    	if(getM_numerador() != 0)
    	{
    		long aux = getM_numerador();
    		setM_numerador(getM_denominador());
    		setM_denominador(aux);
    	}
    	else
    		throw new ArithmeticException("No se puede hacer la operaci�n con una fracci�n nula");
    	return this;
    }
    /**
     * M�todo para elevar una fracci�n a una potencia
     * @param a_exponete recibe por par�meto el n�mero al que se desea elevar 
     * @return una nueva fracci�n con la operaci�n resultante
     */
    public Fraction power(int a_exponente)
    {
    	if(a_exponente < 0)
    	{
    		a_exponente = a_exponente * (-1);
    		reciprocal();
    	}
    	m_numerador = (long)(Math.pow(getM_numerador(), a_exponente));
        m_denominador = (long)(Math.pow(getM_denominador(), a_exponente));
    	return this;
    }   
	/**
	 * M�todo que compara 2 fracciones y determina si son iguales
	 * @param recibe, como objeto al ser la sobrecarga de equals del sistema, un objeto el cual ser�, luego,
	 * una fraccion
	 * @return devuelve true si el contenido de las fracciones son iguales, false en el caso de que no lo sean
	 */
    @Override
	public boolean equals(Object a_fracObj)
	{
        Fraction t_fraction = (Fraction) a_fracObj;
        return m_numerador * t_fraction.getM_denominador() == m_denominador * t_fraction.getM_numerador();
	}
	/**
	 * M�todo que compara 2 fracciones y determina si la fraccion que la llama es menor que la pasada por p�rametro
	 * @param t_fraction recibe la fracci�n con la que se compara
	 * @return devueve true si es menor, false si no lo es
	 */
	public boolean less(Fraction t_fraction)
	{
		return m_numerador * t_fraction.getM_denominador() < m_denominador * t_fraction.getM_numerador();
	}
	/**
	 * M�todo que compara 2 fracciones y determina si la ffracci�n que la llama es menor o igual a la pasada por p�rametro
	 * @param t_fraction recibe la fracci�n con la que se compara
	 * @return devuelve true si es menor o igual, false si es mayor.
	 */
	public boolean lessEquals(Fraction t_fraction)
	{
	    return m_numerador * t_fraction.getM_denominador() <= m_denominador * t_fraction.getM_numerador();
	}
	/**
	 * M�todo que determina si una fracci�n es propia o no
	 * @return devuelve true si es una fracci�n propia, false si no lo se
	 */
	public boolean isProper()
	{
		if(getM_numerador() < getM_denominador())
			return true;
		else
			return false;
	}
	/**
	 * M�tode que recibe dos fracciones y devuelve la fracci�n mayor de las dos
	 * @param a_frac1
	 * @param a_frac2
	 * @return
	 */
	public static Fraction maximoFracciones(Fraction a_frac1, Fraction a_frac2)
	{
	    return a_frac1.less(a_frac2) ? a_frac2: a_frac1;
	}
	
	public static long maxComDivEntreDosFrac(Fraction a_frac1, Fraction a_frac2)
    {
        long t_mcdF1 = Fraction.gcd(a_frac1.getM_numerador(), a_frac1.getM_denominador());
        long t_mcdF2 = Fraction.gcd(a_frac2.getM_numerador(), a_frac2.getM_denominador());
        return t_mcdF1 >= t_mcdF2 ? t_mcdF1 : t_mcdF2;
    }
	/**
	 * M�todo que clona la fracci�n,
	 * se retorna a si misma
	 */
	@Override
    public Fraction clone() throws CloneNotSupportedException
    {
        return new Fraction(m_numerador,m_denominador);
    }
    @Override
    public String toString()
    {
       /* String t_separador = "\n";
        String t_num = Long.toString(getM_numerador());
        String t_den = Long.toString(getM_denominador());
        int t_hasta=0 ;
        if(t_num.length() > t_den.length())
            t_hasta = t_num.length();
        else
            t_hasta = t_den.length();
        
        for (int i = 0; i < t_hasta; i++)
        {
            t_separador += "-";
        }
        t_separador += "\n";
        return getM_numerador() + t_separador + getM_denominador(); 
        */
        
        return getM_numerador() + "/" + getM_denominador();
    }
	/**
	 * M�todo privado que simplifica la fracci�n
	 * @return una referencia de la propia fracci�n
	 */
    private Fraction simplificar()
    {
        long t_mcd = gcd(m_numerador,m_denominador);
        m_numerador /= t_mcd;
        m_denominador /= t_mcd;
        return this;
    }
    /**
     * M�todo que normaliza la funci�n
     * @return retorna una referencia de la misma fracci�n
     */
    private Fraction normaliza()
    {
        if (this.m_denominador != 0)
        {
            if(this.m_denominador < 0)
            {
                this.m_denominador *= -1L;
                this.m_numerador *= -1L;
            }
        } else
            throw new IllegalArgumentException("\nEl denominador tiene que ser mayor que 0");
        return this;
    }
    // Getter's & Setter's
    public void set_fraction(Fraction a_fraccion)
    {
        setM_numerador(a_fraccion.getM_numerador());
        setM_denominador(a_fraccion.getM_denominador());
    }
    public long getM_numerador()
    {
        return m_numerador;
    }
    public void setM_numerador(long m_numerador)
    {
        this.m_numerador = m_numerador;
        simplificar();
    }
    public long getM_denominador()
    {
        return m_denominador;
    }
    public void setM_denominador(long a_denominador)
    {
        this.m_denominador = a_denominador;
        simplificar();
        normaliza();
    }
}
