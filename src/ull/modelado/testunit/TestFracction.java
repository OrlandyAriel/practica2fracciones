package ull.modelado.testunit;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import ull.modelado.fraccion.Fraction;

public class TestFracction
{
	private Fraction m_fraccion1;
	private Fraction m_fraccion2;
	@Before
	public void inicio()
	{
		m_fraccion1 = new Fraction(4,5);
		m_fraccion2 = new Fraction(3,2);
	}
	//Operaciones con fracciones
	@Test
	public void sumaFraccion()
	{
		Fraction t_aux = new Fraction(23, 10);
		Fraction t_result = m_fraccion1.add(m_fraccion2);
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void restaFraccion() throws CloneNotSupportedException
	{
		Fraction t_aux = new Fraction(-7, 10);
		Fraction t_result = m_fraccion1.minus(m_fraccion2);
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void multiplicacionFraccion()
	{
		Fraction t_aux = new Fraction(6, 5);
		Fraction t_result = m_fraccion1.multiply(m_fraccion2);
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void divisionFraccion()
	{
		Fraction t_aux = new Fraction(8, 15);
		Fraction t_result = m_fraccion1.divide(m_fraccion2);
		assertTrue(t_aux.equals(t_result));
	}
	//Operaciones con n�meros enteros
	public void sumaNumero()
	{
		Fraction t_aux = new Fraction(19, 5);
		Fraction t_result = m_fraccion1.add(new Fraction(3));
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void restaNumero() throws CloneNotSupportedException
	{
		Fraction t_aux = new Fraction(-7,10);
		Fraction t_result = m_fraccion1.minus(m_fraccion2);
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void multiplicacionNumero()
	{
		Fraction t_aux = new Fraction(12, 5);
		Fraction t_result = m_fraccion1.multiply(new Fraction(3));
		assertTrue(t_aux.equals(t_result));
	}
	@Test
	public void divisionNumero()
	{
		Fraction t_aux = new Fraction(4,15);
		Fraction t_result = m_fraccion1.divide(new Fraction(3));
		assertTrue(t_aux.equals(t_result));
	}
	//Otras funcionalidades
	@Test
	public void cambioSigno()
	{
		m_fraccion1.changeSign();
		assertTrue(m_fraccion1.equals(new Fraction(-4,5)));
	}
	@Test
	public void ratio()
	{
		assertTrue(m_fraccion1.ratio() == 0.8);
	}
	@Test
	public void potencia()
	{
		Fraction t_axu = m_fraccion1.power(2);
		assertTrue(t_axu.equals(new Fraction(16,25)));
	}
	@Test
	public void reciproco()
	{
		 m_fraccion2.reciprocal();
		 assertTrue(m_fraccion2.equals(new Fraction(2,3)));
	}
	@Test
	public void propiedad()
	{
		assertTrue(m_fraccion1.isProper() == true);
	}
	@Test
	public void menorQue()
	{
		assertTrue(m_fraccion1.less(m_fraccion2));
	}
	@Test
	public void igualdad()
	{
		assertTrue(m_fraccion1.equals(new Fraction(4,5)));
	}
	public void menorIgual()
	{
		assertTrue(m_fraccion1.lessEquals(m_fraccion2));
	}
}
